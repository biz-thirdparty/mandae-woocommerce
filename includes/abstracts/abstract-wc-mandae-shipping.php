<?php
/**
 * Abstract Mandae shipping method.
 *
 * @package WooCommerce_Mandae/Abstracts
 * @since   3.0.0
 * @version 1.0.2
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Default Mandae shipping method abstract class.
 *
 * This is a abstract method with default options for all methods.
 */
abstract class WC_Mandae_Shipping extends WC_Shipping_Method
{
    /**
     * Initialize the Mandae shipping method.
     *
     * @param int $instance_id Shipping zone instance ID.
     */
    public function __construct($instance_id = 0)
    {
        $this->instance_id = absint($instance_id);
        $this->method_description = sprintf(__('%s is a shipping method from Mandae.', 'woocommerce-mandae'), $this->method_title);
        $this->supports = array(
            'shipping-zones',
            'instance-settings',
            'instance-settings-modal'
        );

        $this->init_form_fields();

        $this->enabled = $this->get_option('enabled');
        $this->title   = $this->get_option('title');
        $this->shipping_class_id = (int) $this->get_option('shipping_class_id', '-1');

        add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
    }

    /**
     * Admin options fields.
     */
    public function init_form_fields()
    {
        $this->instance_form_fields = array(
            'enabled' => array(
                'title'   => __('Enable/Disable', 'woocommerce-mandae'),
                'type'    => 'checkbox',
                'label'   => __('Enable this shipping method', 'woocommerce-mandae'),
                'default' => 'yes',
            ), //valor declarado
            'title' => array(
                'title'       => __('Title', 'woocommerce-mandae'),
                'type'        => 'text',
                'description' => __("This controls the title which the user sees during checkout.", 'woocommerce-mandae'),
                'desc_tip'    => true,
                'default'     => $this->method_title,
            )
        );
    }

    /**
     * Mandae options page.
     */
    public function admin_options()
    {
        include WC_Mandae::get_plugin_path() . 'includes/admin/views/html-admin-shipping-method-settings.php';
    }

    /**
     * Get shipping rate.
     *
     * @param  array $package Cart package.
     *
     * @return SimpleXMLElement|null
     */
    protected function get_rate($package)
    {
        $cart = $this->get_cart_items();

        $api = new WC_Mandae_Webservice($this->id, $this->instance_id);
        $api->set_package($package);

        $api->log->add($this->id, 'Instance: ' . $this->instance_id);

        $api->set_debug(get_option( 'woocommerce_mandae-integration_settings', $default = false )['tracking_debug']);
        $api->set_auth(get_option( 'woocommerce_mandae-integration_settings', $default = false )['auth']);
        $api->set_environment(get_option( 'woocommerce_mandae-integration_settings', $default = false )['environment']);
        $api->set_destination_postcode($package['destination']['postcode']);

        $declared_value = 0;

        if (get_option('woocommerce_mandae-integration_settings', $default = false )['declared_value']) {
            $firstItem = array_shift($cart);
            $declared_value = $firstItem['line_total'];
        }

        $api->set_declared_value($declared_value);

        $shipping = $api->get_shipping();

        return $shipping;
    }

    /**
     * Check if package uses only the selected shipping class.
     *
     * @param  array $package Cart package.
     * @return bool
     */
    protected function has_only_selected_shipping_class($package)
    {
        $only_selected = true;

        if (-1 === $this->shipping_class_id) {
            return $only_selected;
        }

        foreach ($package['contents'] as $item_id => $values) {
            $product = $values['data'];
            $qty = $values['quantity'];

            if ($qty > 0 && $product->needs_shipping()) {
                if ($this->shipping_class_id !== $product->get_shipping_class_id()) {
                    $only_selected = false;
                    break;
                }
            }
        }

        return $only_selected;
    }

    /**
     * Calculates the shipping rate.
     *
     * @param array $package Order package.
     */
    public function calculate_shipping($package = array())
    {
        if ('' === $package['destination']['postcode']) {
            return;
        }

        if (!$this->has_only_selected_shipping_class($package)) {
            return;
        }

        $shipping = json_decode($this->get_rate($package));

        $label = null;//json_encode($shipping->shippingServices);
        $days  = null;
        $cost  = null;

        if ($shipping) {
            foreach ($shipping->shippingServices as $shippingService) {

                $id = 'woocommerce_' . $this->id;

                $value = $this->get_wp_option($id);


                if (
                    property_exists($this, 'enabled')
                    && ($this->enabled != 'yes' && $this->enabled != '1')
                ){
                    continue;
                }

                if ($shippingService->name == $this->method_code) {
                    $label = $shippingService->name;
                    $days  = $shippingService->days;
                    $cost  = $shippingService->price;
                    break;
                }

            }
        }

        $add_delivery_time = get_option( 'woocommerce_mandae-integration_settings', $default = false )['add_delivery_time'];

        if ($add_delivery_time) {
            $days += $add_delivery_time;
        }

        if ($label) {
            $label = $this->title . ' - ' . $days . ' dia(s)';

            $rate = apply_filters('woocommerce_mandae_' . $this->id . '_rate', array(
                'id' => $this->id . $this->instance_id,
                'label' => $label,
                'cost' => (float)$cost + (float)get_option('woocommerce_mandae-integration_settings', $default = false)['handling_fee'],
            ), $this->instance_id, $package);

            $rates = apply_filters('woocommerce_mandae_shipping_methods', array($rate), $package);

            $this->add_rate($rates[0]);
        }
    }

    /**
     * @return mixed
     */
    private function get_cart_items()
    {
        global $woocommerce;

        return $woocommerce->cart->get_cart();
    }

    /**
     * @param $item
     * @param array $value
     * @return float
     */
    private function get_item_values($item, $value)
    {
        $productMeta = (float) get_post_meta($item['product_id'] , $value, true);
        $qty = (float) $item['quantity'];
        return (float) ($productMeta * $qty);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function get_wp_option($id)
    {
        global $wpdb;

        $row = $wpdb->get_row($wpdb->prepare("SELECT * FROM `{$wpdb->options}` WHERE `option_name` LIKE '%". $id. "%'", ''));

        return unserialize($row->option_value);
    }

    private function removeAccents($string, $german=false)
    {
        static $replacements;

        if (empty($replacements[$german])) {
            $subst = array(
                // single ISO-8859-1 letters
                192=>'A', 193=>'A', 194=>'A', 195=>'A', 196=>'A', 197=>'A', 199=>'C',
                208=>'D', 200=>'E', 201=>'E', 202=>'E', 203=>'E', 204=>'I', 205=>'I',
                206=>'I', 207=>'I', 209=>'N', 210=>'O', 211=>'O', 212=>'O', 213=>'O',
                214=>'O', 216=>'O', 138=>'S', 217=>'U', 218=>'U', 219=>'U', 220=>'U',
                221=>'Y', 142=>'Z', 224=>'a', 225=>'a', 226=>'a', 227=>'a', 228=>'a',
                229=>'a', 231=>'c', 232=>'e', 233=>'e', 234=>'e', 235=>'e', 236=>'i',
                237=>'i', 238=>'i', 239=>'i', 241=>'n', 240=>'o', 242=>'o', 243=>'o',
                244=>'o', 245=>'o', 246=>'o', 248=>'o', 154=>'s', 249=>'u', 250=>'u',
                251=>'u', 252=>'u', 253=>'y', 255=>'y', 158=>'z',
                // HTML entities
                258=>'A', 260=>'A', 262=>'C', 268=>'C', 270=>'D', 272=>'D', 280=>'E',
                282=>'E', 286=>'G', 304=>'I', 313=>'L', 317=>'L', 321=>'L', 323=>'N',
                327=>'N', 336=>'O', 340=>'R', 344=>'R', 346=>'S', 350=>'S', 354=>'T',
                356=>'T', 366=>'U', 368=>'U', 377=>'Z', 379=>'Z', 259=>'a', 261=>'a',
                263=>'c', 269=>'c', 271=>'d', 273=>'d', 281=>'e', 283=>'e', 287=>'g',
                305=>'i', 322=>'l', 314=>'l', 318=>'l', 324=>'n', 328=>'n', 337=>'o',
                341=>'r', 345=>'r', 347=>'s', 351=>'s', 357=>'t', 355=>'t', 367=>'u',
                369=>'u', 378=>'z', 380=>'z',
                // ligatures
                198=>'Ae', 230=>'ae', 140=>'Oe', 156=>'oe', 223=>'ss',
            );

            if ($german) {
                // umlauts
                $subst = array_merge($subst, array(
                    196=>'Ae', 228=>'ae', 214=>'Oe', 246=>'oe', 220=>'Ue', 252=>'ue'
                ));
            }

            $replacements[$german] = array();
            foreach ($subst as $k=>$v) {
                $replacements[$german][$k<256 ? chr($k) : '&#'.$k.';'] = $v;
            }
        }

        // convert string from default database format (UTF-8)
        // to encoding which replacement arrays made with (ISO-8859-1)
        if ($s = @iconv('UTF-8', 'ISO-8859-1', $string)) {
            $string = $s;
        }

        // Replace
        $string = strtr($string, $replacements[$german]);

        return $string;
    }
}
