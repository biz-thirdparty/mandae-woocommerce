<?php
/**
 * Mandae SEDEX shipping method.
 *
 * @package WooCommerce_Mandae/Classes/Shipping
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * SEDEX shipping method class.
 */
class WC_Mandae_Shipping_Rapido extends WC_Mandae_Shipping
{
    /**
     * Initialize SEDEX.
     *
     * @param int $instance_id Shipping zone instance.
     */
    public function __construct($instance_id = 0)
    {
        $this->id = 'mandae-rapido';
        $this->method_title = __('Mandaê - Rápido', 'woocommerce-mandae');
        $this->method_code = 'Rápido';

        parent::__construct($instance_id);
    }
}
