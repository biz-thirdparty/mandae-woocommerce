<?php
/**
 * Mandae Webservice.
 *
 * @package WooCommerce_Mandae/Classes/Webservice
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Mandae Webservice integration class.
 */
class WC_Mandae_Webservice
{
    const SANDBOX_URL = 'https://sandbox.api.mandae.com.br/';

    const LIVE_URL = 'https://api.mandae.com.br/';

    /**
     * Shipping method ID.
     *
     * @var string
     */
    protected $id = '';

    /**
     * Shipping zone instance ID.
     *
     * @var int
     */
    protected $instance_id = 0;

    /**
     * Authorization.
     *
     * @var string
     */
    protected $auth = '';

    /**
     * Authorization.
     *
     * @var string
     */
    protected $environment = '';

    /**
     * WooCommerce package containing the products.
     *
     * @var array
     */
    protected $package = null;

    /**
     * Destination postcode.
     *
     * @var string
     */
    protected $destination_postcode = '';

    /**
     * Package height.
     *
     * @var float
     */
    protected $height = 0;

    /**
     * Package width.
     *
     * @var float
     */
    protected $width = 0;

    /**
     * Package length.
     *
     * @var float
     */
    protected $length = 0;

    /**
     * Package weight.
     *
     * @var float
     */
    protected $weight = 0;

    /**
     * Declared value.
     *
     * @var string
     */
    protected $declared_value = '0';

    /**
     * Debug mode.
     *
     * @var string
     */
    protected $debug = 'no';

    /**
     * Logger.
     *
     * @var WC_Logger
     */
    public $log = null;

    /**
     * Initialize webservice.
     *
     * @param string $id Method ID.
     * @param int $instance_id Instance ID.
     */
    public function __construct($id = 'mandae', $instance_id = 0)
    {
        $this->id          = $id;
        $this->instance_id = $instance_id;
        $this->log         = new WC_Logger();
    }

    /**
     * Set auth.
     *
     * @param string $auth Authorization.
     */
    public function set_auth($auth = '')
    {
        $this->auth = $auth;
    }

    /**
     * Set environment.
     *
     * @param string $environment Environment.
     */
    public function set_environment($environment = '')
    {
        $this->environment = $environment;
    }

    /**
     * Set shipping package.
     *
     * @param array $package Shipping package.
     */
    public function set_package($package = array())
    {
        $this->package = $package;
        $mandae_package = new WC_Mandae_Package($package);

        if (!is_null($mandae_package)) {
            $data = $mandae_package->get_data();

            $this->set_height($data['height']);
            $this->set_width($data['width']);
            $this->set_length($data['length']);
            $this->set_weight($data['weight']);
        }

        if ('yes' === $this->debug) {
            if (!empty($data)) {
                $data = array(
                    'weight' => $this->get_weight(),
                    'height' => $this->get_height(),
                    'width' => $this->get_width(),
                    'length' => $this->get_length(),
                );
            }

            $this->log->add($this->id, 'Weight and cubage of the order: ' . print_r($data, true));
        }
    }

    /**
     * Set destination postcode.
     *
     * @param string $postcode Destination postcode.
     */
    public function set_destination_postcode($postcode = '')
    {
        $this->destination_postcode = $postcode;
    }

    /**
     * Set shipping package height.
     *
     * @param float $height Package height.
     */
    public function set_height($height = 0)
    {
        $this->height = (float)$height;
    }

    /**
     * Set shipping package width.
     *
     * @param float $width Package width.
     */
    public function set_width($width = 0)
    {
        $this->width = (float)$width;
    }

    /**
     * Set shipping package length.
     *
     * @param float $length Package length.
     */
    public function set_length($length = 0)
    {
        $this->length = (float)$length;
    }

    /**
     * Set shipping package weight.
     *
     * @param float $weight Package weight.
     */
    public function set_weight($weight = 0)
    {
        $this->weight = (float)$weight;
    }

    /**
     * Set declared value.
     *
     * @param string $declared_value Declared value.
     */
    public function set_declared_value($declared_value = '0')
    {
        $this->declared_value = $declared_value;
    }

    /**
     * Set the debug mode.
     *
     * @param string $debug Yes or no.
     */
    public function set_debug($debug = 'no')
    {
        $this->debug = $debug;
    }

    /**
     * Get authorization.
     *
     * @return string
     */
    public function get_auth()
    {
        return apply_filters('woocommerce_mandae_auth', $this->auth, $this->id, $this->instance_id, $this->package);
    }

    /**
     * Get environment.
     *
     * @return string
     */
    public function get_environment()
    {
        return apply_filters('woocommerce_mandae_environment', $this->environment, $this->id, $this->instance_id, $this->package);
    }

    /**
     * Get height.
     *
     * @return float
     */
    public function get_height()
    {
        return $this->float_to_string($this->height);
    }

    /**
     * Get width.
     *
     * @return float
     */
    public function get_width()
    {
        return $this->float_to_string($this->width);
    }

    /**
     * Get length.
     *
     * @return float
     */
    public function get_length()
    {
        return $this->float_to_string($this->length);
    }

    /**
     * Get weight.
     *
     * @return float
     */
    public function get_weight()
    {
        return $this->float_to_string($this->weight);
    }

    /**
     * Fix number format for XML.
     *
     * @param  float $value Value with dot.
     *
     * @return string        Value with comma.
     */
    protected function float_to_string($value)
    {
        $value = str_replace('.', ',', $value);

        return $value;
    }

    /**
     * @param $environment
     * @return string
     */
    private function get_url($environment)
    {
        if ($environment == 'sandbox') {
            return self::SANDBOX_URL;
        }

        return self::LIVE_URL;
    }

    /**
     * Get shipping prices.
     *
     * @return SimpleXMLElement|array
     */
    public function get_shipping()
    {
        $url = $this->get_url($this->environment);

        $url .= 'v2/postalcodes/' . str_replace("-", "", $this->destination_postcode) . '/rates';

        $body = json_encode(
            array(
                'declaredValue' => $this->declared_value,
                'weight' => $this->weight,
                'height' => $this->height,
                'width'  => $this->width,
                'length' => $this->length
            )
        );

        $this->log->add($this->id, 'BODY: ' . $body);
        $shipping = null;

        try {
            $response = wp_remote_post(
                $url,
                array(
                    'headers' => array(
                        'Content-Type'  => 'application/json',
                        'Authorization' => $this->auth
                    ),
                    'body' => $body
                )
            );

            $this->log->add($this->id, json_encode($response));

            if (isset($response['response']) && $response['response']['code'] == 200) {
                $shipping = $response['body'];
            }
        } catch (Exception $e) {
            $this->log->add($this->id, $e->getMessage());
            return null;
        }

        return $shipping;
    }
}
