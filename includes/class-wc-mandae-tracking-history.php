<?php
/**
 * Mandae Tracking History.
 *
 * @package WooCommerce_Mandae/Classes/Tracking
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Mandae tracking history class.
 */
class WC_Mandae_Tracking_History
{
    const SANDBOX_URL = 'https://sandbox.api.mandae.com.br/';

    const LIVE_URL = 'https://api.mandae.com.br/';

    /**
     * Tracking webservice URL.
     *
     * @var string
     */
    private $_webservice_url = 'https://webservice.mandae.com.br/service/rastro/Rastro.wsdl';

    /**
     * Initialize actions.
     */
    public function __construct()
    {
        add_action('woocommerce_order_details_after_order_table', array($this, 'view'), 1);
    }

    /**
     * Get the tracking history webservice URL.
     *
     * @return string
     */
    protected function get_tracking_history_webservice_url()
    {
        return apply_filters('woocommerce_mandae_tracking_webservice_url', $this->_webservice_url);
    }

    /**
     * Get user data.
     *
     * @return array
     */
    protected function get_user_data()
    {
        $user_data = apply_filters('woocommerce_mandae_tracking_user_data', array('login' => 'ECT', 'password' => 'SRO'));

        return $user_data;
    }

    /**
     * Logger.
     *
     * @param string $data Data to log.
     */
    protected function logger($data)
    {
        if (apply_filters('woocommerce_mandae_enable_tracking_debug', false)) {
            $logger = new WC_Logger();
            $logger->add('mandae-tracking-history', $data);
        }
    }

    /**
     * Access API Mandae.
     *
     * @param  array $tracking_code Tracking code.
     *
     * @return array
     */
    protected function get_tracking_history($tracking_code)
    {
        $this->logger(sprintf('Fetching tracking history for "%s" on Mandae Webservices...', $tracking_code));

        $url = $this->get_url(get_option( 'woocommerce_mandae-integration_settings', $default = false )['environment']);

        $url .= 'v2/trackings/' . $tracking_code;

        try {
            $response = wp_remote_get(
                $url,
                array(
                    'headers' => array(
                        'Content-Type'  => 'application/json',
                        'Authorization' => get_option( 'woocommerce_mandae-integration_settings', $default = false )['auth']
                    )
                )
            );

            $trackingHistory = null;

            if ($response['response']['code'] == 200) {
                $trackingHistory = json_decode($response['body']);
            }
        } catch (Exception $e) {
            $trackingHistory = null;
            $this->logger(sprintf('An error occurred while trying to fetch the tracking history for "%s": %s', $tracking_code, $e->getMessage()));
        }

        if (!is_null($trackingHistory)) {
            $this->logger(sprintf('Tracking history found successfully: %s', print_r($trackingHistory, true)));
        }

        return apply_filters('woocommerce_mandae_tracking_objects', $trackingHistory, $tracking_code);
    }

    /**
     * Display the order tracking code in order details and the tracking history.
     *
     * @param WC_Order $order Order data.
     */
    public function view($order)
    {
        $objects = array();

        $tracking_codes = wc_mandae_get_tracking_codes($order);

        if (empty($tracking_codes)) {
            return;
        }

        wc_get_template(
            'myaccount/tracking-title.php',
            array(),
            '',
            WC_Mandae::get_templates_path()
        );

        foreach ($tracking_codes as $tracking_code) {
            if (apply_filters('woocommerce_mandae_enable_tracking_history', false)) {
                $objects = $this->get_tracking_history($tracking_code);
            }

            if (!empty($objects)) {
                wc_get_template(
                    'myaccount/tracking-history-table.php',
                    array(
                        'events' => (array) $objects->events,
                        'trackingCode' => (string) $objects->trackingCode,
                        'carrierCode'  => (string) $objects->carrierCode,
                        'carrierName'  => (string) $objects->carrierName,
                    ),
                    '',
                    WC_Mandae::get_templates_path()
                );
            }
        }
    }

    /**
     * @param $environment
     * @return string
     */
    private function get_url($environment)
    {
        if ($environment == 'sandbox') {
            return self::SANDBOX_URL;
        }

        return self::LIVE_URL;
    }
}

new WC_Mandae_Tracking_History();