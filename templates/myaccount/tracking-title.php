<?php
/**
 * Tracking title.
 *
 * @author  BizCommerce
 * @package WooCommerce_Mandae/Templates
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<h2 id="#wc-mandae-tracking"
    class="wc-mandae-tracking__title"><?php esc_html_e('Mandae delivery tracking', 'woocommerce-mandae'); ?></h2>