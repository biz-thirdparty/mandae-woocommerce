<?php
/**
 * Shipping methods admin settings.
 *
 * @package WooCommerce_Mandae/Admin/Settings
 */

if (!defined('ABSPATH')) {
    exit;
}

$suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
wp_enqueue_script('wc-mandae', plugins_url('assets/js/admin/shipping-methods' . $suffix . '.js', WC_Mandae::get_main_file()), array('jquery'), WC_Mandae::VERSION, true);

echo $this->get_admin_options_html();