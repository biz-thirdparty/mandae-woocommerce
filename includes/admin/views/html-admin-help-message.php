<?php
/**
 * Admin help message.
 *
 * @package WooCommerce_Mandae/Admin/Settings
 */

if (!defined('ABSPATH')) {
    exit;
}

if (apply_filters('woocommerce_mandae_help_message', true)) : ?>
    <div class="updated woocommerce-message inline">
        <p><?php echo esc_html(sprintf(__('Help us keep the %s plugin free making a donation or rate &#9733;&#9733;&#9733;&#9733;&#9733; on WordPress.org. Thank you in advance!', 'woocommerce-mandae'), __('WooCommerce Mandae', 'woocommerce-mandae'))); ?></p>
        <p><a href="https://claudiosanches.com/doacoes/" target="_blank"
              class="button button-primary"><?php esc_html_e('Make a donation', 'woocommerce-mandae'); ?></a> <a
                    href="https://wordpress.org/support/plugin/woocommerce-mandae/reviews/?filter=5#new-post"
                    target="_blank"
                    class="button button-secondary"><?php esc_html_e('Make a review', 'woocommerce-mandae'); ?></a>
        </p>
    </div>
<?php endif;
