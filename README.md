# Mandaê shipping method module for WooCommerce.

Shipping method module mandaê (v1.0.7) - WooCommerce


**[Mandaê](https://www.mandae.com.br/)** 

With this module you'll be able to:
 - Estimate Shipping Rates using **Mandaê**
 - Track your packages in WooCommerce. 

## Installation


### Manual:

- Download the [latest version](https://bitbucket.org/biz-thirdparty/mandae-woocommerce/downloads/) of the module.
- Unzip the file, copy and paste into yout Magento folder.
- Clear the Cache.
- Fill your Mandaê Auth Token and that's it.


**[Documentação](https://dev.mandae.com.br/api/index.html)**
**[Dúvidas](https://mandae.zendesk.com/hc/pt-br/requests/new)**

