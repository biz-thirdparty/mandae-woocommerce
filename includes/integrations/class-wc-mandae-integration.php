<?php
/**
 * Mandae integration.
 *
 * @package WooCommerce_Mandae/Classes/Integration
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Mandae integration class.
 */
class WC_Mandae_Integration extends WC_Integration
{

    /**
     * Initialize integration actions.
     */
    public function __construct()
    {
        $this->id = 'mandae-integration';
        $this->method_title = __('Mandae', 'woocommerce-mandae');

        $this->init_form_fields();

        $this->init_settings();

        $this->tracking_enable = $this->get_option('tracking_enable');
        $this->tracking_debug = $this->get_option('tracking_debug');

        $this->auth = $this->get_option('auth');
        $this->environment = $this->get_option('environment');
        $this->declared_value = $this->get_option('declared_value');

        add_action('woocommerce_update_options_integration_' . $this->id, array($this, 'process_admin_options'));

        // Tracking history actions.
        add_filter('woocommerce_mandae_enable_tracking_history', array($this, 'setup_tracking_history'), 10);
        add_filter('woocommerce_mandae_enable_tracking_debug', array($this, 'setup_tracking_debug'), 10);
    }

    /**
     * Get tracking log url.
     *
     * @return string
     */
    protected function get_tracking_log_link()
    {
        return ' <a href="' . esc_url(admin_url('admin.php?page=wc-status&tab=logs&log_file=mandae-tracking-history-' . sanitize_file_name(wp_hash('mandae-tracking-history')) . '.log')) . '">' . __('View logs.', 'woocommerce-mandae') . '</a>';
    }

    /**
     * Initialize integration settings fields.
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'tracking' => array(
                'title'       => __('Tracking History Table', 'woocommerce-mandae'),
                'type'        => 'title',
                'description' => __('Displays a table with informations about the shipping in My Account > View Order page.', 'woocommerce-mandae'),
            ),
            'tracking_enable' => array(
                'title'   => __('Enable/Disable', 'woocommerce-mandae'),
                'type'    => 'checkbox',
                'label'   => __('Enable Tracking History Table', 'woocommerce-mandae'),
                'default' => 'no',
            ),
            'auth' => array(
                'title'       => __('Authorization', 'woocommerce-mandae'),
                'type'        => 'text',
                'description' => __("You do not need to authenticate to use the API. However, for requests to be authorized, a valid token must be sent on all requests made to the API through the HTTP Authorization header.\nYour API tokens can be accessed in your → API Settings within the web application.", 'woocommerce-mandae'),
                'desc_tip'    => true,
            ),
            'environment' => array(
                'title'       => __('Environment', 'woocommerce-mandae'),
                'type'        => 'select',
                'class'       => 'wc-enhanced-select',
                'default'     => 'live',
                'options'     => array(
                    'sandbox' => __('Sandbox', 'woocommerce-mandae'),
                    'live'    => __('Live', 'woocommerce-mandae'),
                ),
            ),
            'declared_value' => array(
                'title'       => __('Declared Value', 'woocommerce-mandae'),
                'type'        => 'select',
                'default'     => 0,
                'options'     => array(
                    0  => __('No', 'woocommerce-mandae'),
                    1  => __('Yes', 'woocommerce-mandae'),
                ),
            ),
            'handling_fee' => array(
                'title'       => __('Handling Fee', 'woocommerce-mandae'),
                'type'        => 'text',
                'default'     => 0
            ),
            'add_delivery_time' => array(
                'title'       => __('Add Delivery Time', 'woocommerce-mandae'),
                'type'        => 'text',
                'default'     => 0
            ),
            'tracking_debug' => array(
                'title'       => __('Debug Log', 'woocommerce-mandae'),
                'type'        => 'checkbox',
                'label'       => __('Enable logging for Tracking History', 'woocommerce-mandae'),
                'default'     => 'no',
                'description' => sprintf(__('Log %s events, such as WebServices requests.', 'woocommerce-mandae'), __('Tracking History Table', 'woocommerce-mandae')) . $this->get_tracking_log_link(),
            )
        );
    }

    /**
     * Mandae options page.
     */
    public function admin_options()
    {
        echo '<h2>' . esc_html($this->get_method_title()) . '</h2>';
        echo wp_kses_post(wpautop($this->get_method_description()));

        echo '<div><input type="hidden" name="section" value="' . esc_attr($this->id) . '" /></div>';
        echo '<table class="form-table">' . $this->generate_settings_html($this->get_form_fields(), false) . '</table>';

        $suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
        wp_enqueue_script($this->id . '-admin', plugins_url('assets/js/admin/integration' . $suffix . '.js', WC_Mandae::get_main_file()), array('jquery', 'jquery-blockui'), WC_Mandae::VERSION, true);
        wp_localize_script(
            $this->id . '-admin',
            'WCMandaeIntegrationAdminParams',
            array(
                'i18n_confirm_message' => __('Are you sure you want to delete all postcodes from the database?', 'woocommerce-mandae'),
                'empty_database_nonce' => wp_create_nonce('woocommerce_mandae_autofill_addresses_nonce'),
            )
        );
    }

    /**
     * Generate Button Input HTML.
     *
     * @param string $key
     * @param array $data
     * @return string
     */
    public function generate_button_html($key, $data)
    {
        $field_key = $this->get_field_key($key);
        $defaults = array(
            'title' => '',
            'label' => '',
            'desc_tip' => false,
            'description' => '',
        );

        $data = wp_parse_args($data, $defaults);

        ob_start();
        ?>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr($field_key); ?>"><?php echo wp_kses_post($data['title']); ?></label>
                <?php echo $this->get_tooltip_html($data); ?>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span><?php echo wp_kses_post($data['title']); ?></span></legend>
                    <button class="button-secondary" type="button"
                            id="<?php echo esc_attr($field_key); ?>"><?php echo wp_kses_post($data['label']); ?></button>
                    <?php echo $this->get_description_html($data); ?>
                </fieldset>
            </td>
        </tr>
        <?php

        return ob_get_clean();
    }

    /**
     * Enable tracking history.
     *
     * @return bool
     */
    public function setup_tracking_history()
    {
        return 'yes' === $this->tracking_enable && class_exists('SoapClient');
    }

    /**
     * Set up tracking debug.
     *
     * @return bool
     */
    public function setup_tracking_debug()
    {
        return 'yes' === $this->tracking_debug;
    }
}
