<?php
/**
 * Mandae Package.
 *
 * @package WooCommerce_Mandae/Classes
 * @since   3.0.0
 * @version 1.0.1
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * WC_Mandae_Package class.
 */
class WC_Mandae_Package
{

    /**
     * Order package.
     *
     * @var array
     */
    protected $package = array();

    /**
     * Sets the package.
     *
     * @param  array $package Package to calcule.
     *
     * @return array
     */
    public function __construct($package = array())
    {
        $this->package = $package;
    }

    /**
     * Extracts the weight and dimensions from the package.
     *
     * @return array
     */
    protected function get_package_data()
    {
        $count = 0;
        $height = array();
        $width = array();
        $length = array();
        $weight = array();

        foreach ($this->package['contents'] as $item_id => $values) {
            $product = $values['data'];
            $qty = $values['quantity'];

            if ($qty > 0 && $product->needs_shipping()) {

                $_height = wc_get_dimension((float)$product->get_length(), 'cm');
                $_width  = wc_get_dimension((float)$product->get_width(), 'cm');
                $_length = wc_get_dimension((float)$product->get_height(), 'cm');
                $_weight = wc_get_weight((float)$product->get_weight(), 'kg');

                $height[$count] = $_height;
                $width[$count]  = $_width;
                $length[$count] = $_length;
                $weight[$count] = $_weight;

                if ($qty > 1) {
                    $n = $count;

                    for ($i = 0; $i < $qty; $i++) {
                        $height[$n] = $_height;
                        $width[$n]  = $_width;
                        $length[$n] = $_length;
                        $weight[$n] = $_weight;
                        $n++;
                    }

                    $count = $n;
                }

                $count++;
            }
        }

        return array(
            'height' => array_values($height),
            'length' => array_values($length),
            'width'  => array_values($width),
            'weight' => array_sum($weight),
        );
    }

    /**
     * Get the max values.
     *
     * @param  array $height Package height.
     * @param  array $width Package width.
     * @param  array $length Package length.
     *
     * @return array
     */
    protected function get_max_values($height, $width, $length)
    {
        $find = array(
            'height' => max($height),
            'width'  => max($width),
            'length' => max($length),
        );

        return $find;
    }

    /**
     * Get the package data.
     *
     * @return array
     */
    public function get_data()
    {
        $data = apply_filters('woocommerce_mandae_default_package', $this->get_package_data());

        return array(
            'height' => apply_filters('woocommerce_mandae_package_height', $data['height']),
            'width'  => apply_filters('woocommerce_mandae_package_width', $data['width']),
            'length' => apply_filters('woocommerce_mandae_package_length', $data['length']),
            'weight' => apply_filters('woocommerce_mandae_package_weight', $data['weight']),
        );
    }
}
